package secretariadeeducacion;


public class Estudiantes extends Persona {

	String Direccion;
	int Curso;
	Boolean Cupo;
	int Edad;
	int Localidad;
	public Estudiantes(int identificacion,String nombres,String apellidos, String direccion, int localidad, int edad,int curso, Boolean cupo) {
		Nombres = nombres;
		Apellidos=apellidos;
		Identificacion = identificacion;
		Edad = edad;
		Localidad=localidad;
		Direccion = direccion;
		Curso = curso;
		Cupo = cupo;
	}
}