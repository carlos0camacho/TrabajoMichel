package secretariadeeducacion;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class SecretrariaDeEducacion {
	public static void main(String[] args) {
		ArrayList<AsignacionCupos> ac = new ArrayList<AsignacionCupos>();
		ArrayList<Estudiantes> E = new ArrayList<Estudiantes>();
		ArrayList<Acudientes> A = new ArrayList<Acudientes>();
		ArrayList<Localidades> L = new ArrayList<Localidades>();
		ArrayList<Colegios> C = new ArrayList<Colegios>();
		ManejoArchivos archivos = new ManejoArchivos();
		int i;
		// datos colegio
		int identificacion;
		String nombrec;
		int noLocalidad;
		// datos estudiantes
		int identificacionE;
		String ApellidoE;
		String nombreE;
		String direccionE;
		int localidadE;
		int edadE;
		int cursoE;
		Boolean cupoE;
		// datos acudiente
		String nombreA;
		int identificacionA;
		String ApellidoA;
		int TelefonoA;
		float IngresosA;
		int ParentescoA;
		// int IdentificacionEstudiante;

		// cargamos los archivos
		// ac=archivos.ListaCuposAs();
		L = archivos.ListaLocalidades();
		C = archivos.ListaColegios();
		E = archivos.ListaEstudiantes();
		A = archivos.ListaAcudientes();

		int Menu;
		do {
			Menu = Integer.parseInt(
					JOptionPane.showInputDialog("\nSeleccione una opcion" + "\n\t1.)Ingresar Estudiante y acudiente"
							+ "\n\t2.)Ingresar Colegio" + "\n\t3.)Asignar cupo" + "\n\t4.)Listar estudiantes inscritos"
							+ "\n\t5.)Listar acudientes" + "\n\t6.)Listar colegios" + "\n\t7.)Listar localidades"
							+ "\n\t8.)Estudiantes sin cupo" + "\n\t9.)Valor promedio de los ingresos acudientes"
							+ "\n\t10Cantidad de cupos en una localidad" + "\n\t11Cantidad de cupos en un colegio"
							+ "\n\t12Verificar estado del estudiante" + "\n\t13.)Guardar Cambios y salir"));
			switch (Menu) {
			case 1:// ingreso de datos
				identificacionE = Integer
						.parseInt(JOptionPane.showInputDialog("Ingrese la identificacion del estudiante"));
				nombreE = JOptionPane.showInputDialog("Ingrese el nombre del estudiante");
				ApellidoE = JOptionPane.showInputDialog("Ingrese el apellido del estudiante");
				direccionE = JOptionPane.showInputDialog("Ingrese la direccion del estudiante");
				localidadE = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el codigo de la localidad"));
				edadE = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la edad del estudiante"));
				cursoE = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el curso del estudiante (0-11)"));
				cupoE = false;
				Estudiantes nuevoE = new Estudiantes(identificacionE, nombreE, ApellidoE, direccionE, localidadE, edadE,
						cursoE, cupoE);
				// ingreso de datos del acudiente
				identificacionA = Integer
						.parseInt(JOptionPane.showInputDialog("Ingrese la identificacion del acudiente"));
				nombreA = JOptionPane.showInputDialog("Ingrese el nombre del acudiente");
				ApellidoA = JOptionPane.showInputDialog("Ingrese el nombre del acudiente");
				TelefonoA = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el telefono"));
				IngresosA = Float.parseFloat(JOptionPane.showInputDialog("Ingrese Los ingresos del acudiente"));
				String opciones[] = { "Padre", "madre" };
				ParentescoA = JOptionPane.showOptionDialog(null, "Ingrese el parentesco", "Acudiente",
						JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, opciones, 0);
				Acudientes nuevoA = new Acudientes(identificacionA, nombreA, ApellidoA, TelefonoA, IngresosA,
						ParentescoA, nuevoE);
				A.add(nuevoA);
				E.add(nuevoE);
				break;
			case 2: // ingresarColegio
				identificacion = Integer.parseInt(JOptionPane.showInputDialog("Ingrese la identificacion del colegio"));
				nombrec = JOptionPane.showInputDialog("Ingrese el nombre del colegio");
				noLocalidad = Integer
						.parseInt(JOptionPane.showInputDialog("Ingrese la identificacion de la localidad del colegio"));
				C.add(new Colegios(identificacion, nombrec, noLocalidad));
				break;
			case 3:// asignarCupo
				break;
			case 4:// ListarEstudiantes
				for (i = 0; i < E.size(); i++) {
					JOptionPane.showMessageDialog(null,
							"Identificacion:\t" + E.get(i).Identificacion + "\n" + "nombres:\t" + E.get(i).Nombres
									+ "\n" + "apellidos:\t" + E.get(i).Apellidos + "\n" + "direccion:\t"
									+ E.get(i).Direccion + "\n" + "localidad:\t" + E.get(i).Localidad + "\n" + "edad:\t"
									+ E.get(i).Edad + "\n" + "curso:\t" + E.get(i).Curso + "\n" + "cupo:\t"
									+ E.get(i).Cupo + "\n");

				}
				break;
			case 5:// ListarAcudientes
				for (i = 0; i < A.size(); i++) {
					JOptionPane.showMessageDialog(null,
							"Identificacion:\t" + A.get(i).Identificacion + "\n" + "nombres:\t" + A.get(i).Nombres
									+ "\n" + "apellidos:\t" + A.get(i).Apellidos + "\n" + "telefono\t"
									+ A.get(i).Telefono + "\n" + "ingresos\t" + A.get(i).Ingresos + "\n"
									+ "Parentesco\t " + A.get(i).Parentesco + "\n" + "Identificacion Estudiante \t"
									+ A.get(i).IdentificacionEstudiante + "\n");
				}
				break;
			case 6:// ListarColegios
				break;
			case 7:// ListarLocalidades
				for (i = 0; i < L.size(); i++) {
					JOptionPane.showMessageDialog(null, "id:" + L.get(i).Identificacion + "Nombre: " + L.get(i).Nombre);
				}

				break;
			case 8:// estudiantes Sin Cupo
				for (i = 0; i < E.size(); i++) {
					if (E.get(i).Cupo == false) {
						JOptionPane.showMessageDialog(null,
								"Identificacion:\t" + E.get(i).Identificacion + "\n" + "nombres:\t" + E.get(i).Nombres
										+ "\n" + "apellidos:\t" + E.get(i).Apellidos + "\n" + "direccion:\t"
										+ E.get(i).Direccion + "\n" + "localidad:\t" + E.get(i).Localidad + "\n"
										+ "edad:\t" + E.get(i).Edad + "\n" + "curso:\t" + E.get(i).Curso + "\n"
										+ "cupo:\t" + E.get(i).Cupo + "\n");
					}
				}
				break;
			case 9:// ingresos promedio
				float suma = 0;
				int contador = 0;
				for (i = 0; i < A.size(); i++) {
					suma = suma + A.get(i).Ingresos;
					contador++;
				}
				JOptionPane.showMessageDialog(null, "El promedio de ingreso es: " + suma / contador);
				break;
			case 10:// cantidad de cupos por localidad
				break;
			case 11:// cantidad de cupos por colegio
				break;
			case 12:// verificar Estado del estudiante
				break;
			case 13:
				archivos.ingresarAlArchivoC(C);
				archivos.ingresarAlArchivoA(A);
				archivos.ingresarAlArchivoE(E);
				archivos.ingresarAlArchivoAc(ac);
				break;
			}
		} while (Menu != 13);
	}
}