package secretariadeeducacion;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class ManejoArchivos {
	// ArrayList<AsignacionCupos> ac= new ArrayList<AsignacionCupos>();
	// ArrayList<Estudiantes> E = new ArrayList<Estudiantes>();
	// ArrayList<Acudientes> A = new ArrayList<Acudientes>();
	// ArrayList<Localidades> L = new ArrayList<Localidades>();
	// ArrayList<Colegios> C = new ArrayList<Colegios>();

	File ficheroEstudiantes = new File("Estudiantes.dat");
	File ficheroColegios = new File("Colegios.txt");
	File ficheroAcudientes = new File("Acudientes.dat");
	File ficheroCupos = new File("Cupos.dat");
	File ficheroLocalidades = new File("Localidades.txt");
	PrintWriter pw;
	DataOutputStream DoS;
	DataInputStream DiS;
	BufferedReader bf;
	StringTokenizer st;

	public void ingresarAlArchivoE(ArrayList<Estudiantes> e) {
		try {
			DoS = new DataOutputStream(new FileOutputStream(ficheroEstudiantes));

			for (int i = 0; i < e.size(); i++) {
				DoS.writeInt(e.get(i).Identificacion);
				DoS.writeUTF(e.get(i).Nombres);
				DoS.writeUTF(e.get(i).Apellidos);
				DoS.writeUTF(e.get(i).Direccion);
				DoS.writeInt(e.get(i).Localidad);
				DoS.writeInt(e.get(i).Edad);
				DoS.writeInt(e.get(i).Curso);
				DoS.writeBoolean(e.get(i).Cupo);
			}
			DoS.close();

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

	}

	public void ingresarAlArchivoA(ArrayList<Acudientes> A) {
		try {
			DoS = new DataOutputStream(new FileOutputStream(ficheroAcudientes));
			for (int i = 0; i < A.size(); i++) {
				DoS.writeInt(A.get(i).Identificacion);
				DoS.writeUTF(A.get(i).Nombres);
				DoS.writeUTF(A.get(i).Apellidos);
				DoS.writeInt(A.get(i).Telefono);
				DoS.writeFloat(A.get(i).Ingresos);
				DoS.writeBoolean(A.get(i).Parentesco);
				DoS.writeInt(A.get(i).IdentificacionEstudiante);
			}
			DoS.close();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void ingresarAlArchivoC(ArrayList<Colegios> c) {
		try {
			pw = new PrintWriter(new FileWriter(ficheroColegios));
			for (int i = 0; i < c.size(); i++) {
				pw.println(c.get(i).Identificacion + ";" + c.get(i).Nombre + ";" + c.get(i).NoLocalidad);
			}
			pw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void ingresarAlArchivoAc(ArrayList<AsignacionCupos> ac) {

		try {
			DoS = new DataOutputStream(new FileOutputStream(ficheroCupos));
			for (int i = 0; i < ac.size(); i++) {
				DoS.writeInt(ac.get(i).nCupo);
				DoS.writeUTF(ac.get(i).Fecha);
				DoS.writeInt(ac.get(i).idEstudiante);
				DoS.writeInt(ac.get(i).IdColegio);
				DoS.writeInt(ac.get(i).noLocalidad);
			}
			DoS.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public ArrayList<Localidades> ListaLocalidades() {
		ArrayList<Localidades> arr = new ArrayList<Localidades>();
		try {
			bf = new BufferedReader(new FileReader(ficheroLocalidades));
			int nLocalidad = 0;
			String linea, nombre;

			while ((linea = bf.readLine()) != null) {
				nombre = linea;
				nLocalidad++;
				arr.add(new Localidades(nLocalidad, nombre));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return arr;

	}

	public ArrayList<Colegios> ListaColegios() {
		ArrayList<Colegios> arr = new ArrayList<Colegios>();
		try {
			bf = new BufferedReader(new FileReader(ficheroColegios));
			String linea, nombre;
			int idColegio, idLocalidad;
			while ((linea = bf.readLine()) != null) {
				st = new StringTokenizer(linea, ";");
				while (st.hasMoreTokens()) {
					idColegio = Integer.parseInt(st.nextToken());
					nombre = st.nextToken();
					idLocalidad = Integer.parseInt(st.nextToken());
					arr.add(new Colegios(idColegio, nombre, idLocalidad));
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return arr;
	}

	public ArrayList<Estudiantes> ListaEstudiantes() {
		ArrayList<Estudiantes> arr = new ArrayList<Estudiantes>();
		try {
			DiS = new DataInputStream(new FileInputStream(ficheroEstudiantes));
			int identificacionE;
			String ApellidoE;
			String nombreE;
			String direccionE;
			int localidadE;
			int edadE;
			int cursoE;
			Boolean cupoE;

			while (true) {
				identificacionE = DiS.readInt();
				ApellidoE = DiS.readUTF();
				nombreE = DiS.readUTF();
				direccionE = DiS.readUTF();
				localidadE = DiS.readInt();
				edadE = DiS.readInt();
				cursoE = DiS.readInt();
				cupoE = DiS.readBoolean();
				arr.add(new Estudiantes(identificacionE, ApellidoE, nombreE, direccionE, localidadE, edadE, cursoE,
						cupoE));
			}

		} catch (EOFException e) {
			System.out.println("Fin de fichero");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return arr;

	}

	public ArrayList<Acudientes> ListaAcudientes() {
		ArrayList<Acudientes> arr = new ArrayList<Acudientes>();
		try {
			DiS = new DataInputStream(new FileInputStream(ficheroAcudientes));
			int identificacion;
			String nombres;
			String apellidos;
			int telefono;
			float ingresos;
			boolean parentesco;
			int idE;

			while (true) {
				identificacion = DiS.readInt();
				nombres = DiS.readUTF();
				apellidos = DiS.readUTF();
				;
				telefono = DiS.readInt();
				ingresos = DiS.readFloat();
				parentesco = DiS.readBoolean();
				idE = DiS.readInt();
				/*
				DoS.writeInt(A.get(i).Identificacion);
				DoS.writeUTF(A.get(i).Nombres);
				DoS.writeUTF(A.get(i).Apellidos);
				DoS.writeInt(A.get(i).Telefono);
				DoS.writeFloat(A.get(i).Ingresos);
				DoS.writeBoolean(A.get(i).Parentesco);
				DoS.writeInt(A.get(i).IdentificacionEstudiante);
				*/
				
				arr.add(new Acudientes(identificacion, nombres, apellidos, telefono, ingresos, parentesco, idE));
			}

		} catch (EOFException e) {
			System.out.println("Fin de fichero");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return arr;
	}
}
