package secretariadeeducacion;

public class Acudientes extends Persona {
	int Telefono;
	float Ingresos;
	Boolean Parentesco;
	int IdentificacionEstudiante;
	Estudiantes E;
//constructor
	public Acudientes(int identificacion,String nombres,String apellidos,int telefono,
			float ingresos, int parentesco, Estudiantes e) {
		super();
		Nombres = nombres;
		Apellidos= apellidos;
		Identificacion = identificacion;
		Telefono = telefono;
		Ingresos = ingresos;
		if(parentesco==0){
			Parentesco = true;			
		}else{
			Parentesco=false;
		}
		IdentificacionEstudiante = e.Identificacion;
		System.out.println("A:acudiente"+IdentificacionEstudiante);

		E = e;
	}	
	public Acudientes(int identificacion,String nombres,String apellidos,int telefono,
			float ingresos, boolean parentesco,int idE) {
		super();
		Nombres = nombres;
		Apellidos= apellidos;
		Identificacion = identificacion;
		Telefono = telefono;
		Ingresos = ingresos;
		Parentesco = parentesco;			
		IdentificacionEstudiante = idE;
		E=null;
	}
}
